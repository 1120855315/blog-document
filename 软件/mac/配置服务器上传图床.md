经常写博客的朋友应该都需要频繁的在博客里插入图片, 然后上传到如 csdn,个人博客, 博客园等地方.

我们的常规操作就是先把图片上传到图床上, 然后再把生成的外链放到博客里, 每张图片都是如此, 这就很麻烦,严重降低了写博客的效率, 那么有没有工具能够简化这个过程的,答案是有的,那就是 iPic,它是一个很方便的图片上传工具,它是mac 平台上口碑最好的一个图床工具, 支持微博图床, 七牛云, 阿里云, 腾讯云,  等常见图床,支持拖拽、快捷键、剪贴板上传，支持上传前压缩，上传完毕自动生成 Markdown 并拷贝到剪贴板。如果你想迁移图床, 可以使用  iPic mover 工具来实现, 这两个工具都可以在 app store 里下载

[下载中文版本typro](http://hao.hnzant.com/index.php?s=/goods/goodsinfo&goodsid=136)

下载好安装完成后按照下图进行安装
![截屏2021-05-11 下午11.00.10.png](http://image.hao.hnzant.com/upload/ueditor/image/20210511/1620745274954928.png)

![截屏2021-05-11 下午11.02.01.png](http://image.hao.hnzant.com/upload/ueditor/image/20210511/1620745390251470.png)

[mac中下载iPic](https://apps.apple.com/cn/app/id1101244278) 点击链接无法下载，可以直接在appstore中进行搜索

![图片.png](http://image.hao.hnzant.com/upload/ueditor/image/20210511/1620745520289345.png)

![图片.png](http://image.hao.hnzant.com/upload/ueditor/image/20210511/1620745675144380.png)

### 配置服务器上传图床 

![图片.png](http://image.hao.hnzant.com/upload/ueditor/image/20210511/1620745931414040.png)

此出选择七牛服务器进行配置，此时会提示试用七天(需要进行iPic无限试用[查看教程](http://hao.hnzant.com/index.php?s=/cms/articleclassinfo&article_id=4))

![图片.png](http://image.hao.hnzant.com/upload/ueditor/image/20210511/1620746058283112.png)

配置七牛云服务器（如果不会配置可以添加QQ：1120855315 进行探讨）

![图片.png](http://image.hao.hnzant.com/upload/ueditor/image/20210511/1620746934929603.png)